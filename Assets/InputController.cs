using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public int count;
    public Spawn from;
    public Spawn to;
    public SpriteRenderer temp;
    public LienRenderer ren;
    public int fromToCount;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider == null)
            {
                if (from != null)
                {
                    fromToCount = 1;
                }
                else
                {
                    fromToCount = 0;
                    from = null;
                    to = null;
                }
                Debug.Log("NO");
                return;
            }
            else if (hit.collider.gameObject.GetComponent<Spawn>() != null)
            {
                fromToCount++;
                if (fromToCount == 1)
                {
                    from = hit.collider.gameObject.GetComponent<Spawn>();

                }
                if (fromToCount == 2)
                {
                    to = hit.collider.gameObject.GetComponent<Spawn>();
                    fromToCount = 0;
                    if (to == from)
                    {
                        to = null;
                        fromToCount = 1;
                    }
                }
                if (from == null || to == null)
                {
                    Debug.Log("NO");
                    return;
                }
                LineRendererUpdate();
                LayerParamUpdate();
                from = null;
                to = null;
            }
        }
    }
    
    public void LineRendererUpdate()
    {
        ren.isEnable = true;
        ren.ratio = 0;
        ren.lr.gameObject.SetActive(true);
    }

    public void LayerParamUpdate()
    {
        temp.sprite = from.fromSprite[from.numberOfLayers - 1].sprite;
        from.numberOfLayers -= 1;
        to.numberOfLayers += 1;
        to.toSprite[to.numberOfLayers - 1].sprite = temp.sprite;
        to.toCount = 0;
        if (from.numberOfLayers == 0)
        {
            from.fromCount = from.balls1.Length - 1;
        }
        if (from.numberOfLayers == 1)
        {
            from.fromCount = from.balls2.Length - 1;
        }
        if (from.numberOfLayers == 2)
        {
            from.fromCount = from.balls3.Length - 1;
        }
        if (from.numberOfLayers == 3)
        {
            from.fromCount = from.balls4.Length - 1;
        }
        if (to.numberOfLayers == 1)
        {
            to.tempI = to.balls1.Length - 1;
        }
        if (to.numberOfLayers == 2)
        {
            to.tempI = to.balls2.Length - 1;
        }
        if (to.numberOfLayers == 3)
        {
            to.tempI = to.balls3.Length - 1;
        }
        if (to.numberOfLayers == 4)
        {
            to.tempI = to.balls4.Length - 1;
        }

        from.isTo = false;
        to.isFrom = false;
        from.isFrom = true;
        to.isTo = true;
    }
}
