using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class LienRenderer : MonoBehaviour
{
    public LineRenderer lr;
    public Spawn spawn;
    public Vector2 point1;
    public Vector2 point2;
    public Vector2 point3;
    public Vector2 point4;

    public float ratio;

    public List<Material> mat;

    public bool isEnable;
    private void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    public void getPosition(Vector2 from)
    {
        point1 = from;
    }
    public void getPosition1(Vector2 to)
    {
        point3 = to;
    }

    public void setColor(int index)
    {
        lr.material = mat[index];

    }

    private void Update()
    {
        point2 = new Vector2((point1.x + point4.x) / 2 + 1, (point1.y + point4.y) / 2 + 2);
        if (isEnable == true)
        {
            ratio++;
            DrawQuadraticBezierCurve(point1, point2, point3);
        }
        if (ratio >= 500)
        {
            lr.gameObject.SetActive(false);
            isEnable = false;
        }

    }

    public void DrawQuadraticBezierCurve(Vector3 point0, Vector3 point1, Vector3 point2)
    {
        lr.positionCount = 200;
        float t = 0f;
        Vector3 B = new Vector3(0, 0, 0);
        for (int i = 0; i < lr.positionCount; i++)
        {
            B = (1 - t) * (1 - t) * point0 + 2 * (1 - t) * t * point1 + t * t * point2;
            lr.SetPosition(i, new Vector3(B.x, B.y, - 2));
            t += (1 / (float)lr.positionCount);
        }
    }
}

