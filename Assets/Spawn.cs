using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Spawn : MonoBehaviour
{
    public GameObject obj;
    public LienRenderer lrenderer;
    public InputController inputController;

    public Transform tempPos;
    public LayerMask layerMask;
    public LayerMask layerMaskDestroy;

    public bool isSpawn;
    public bool isStartSpawn;

    public int fromCount = 0;
    public int toCount = 0;
    public List<Vector3> listPos = new List<Vector3>();
    public List<GameObject> listBall = new List<GameObject> ();

    public List<SpriteRenderer> listSprite;
    public SpriteRenderer sprite1;
    public SpriteRenderer sprite2;
    public SpriteRenderer sprite3;
    public SpriteRenderer sprite4;

    public float currTime;
    public float countDown;

    public GameObject group1;
    public GameObject group2;
    public GameObject group3;
    public GameObject group4;

    public List<SpriteRenderer> fromSprite;
    public List<SpriteRenderer> toSprite;

    public int numberOfLayers;
    public int currLayers;

    public bool isFrom;
    public bool isTo;
    public SpriteRenderer[] balls1;
    public SpriteRenderer[] balls2;
    public SpriteRenderer[] balls3;
    public SpriteRenderer[] balls4;

    public Vector3 defaultScale;
    public int tempI;

    public int rand1, rand2, rand3, rand4;
    private void Start()
    {
        currLayers = numberOfLayers;
        GetListPos();

        rand1 = Random.Range(0, listSprite.Count);
        rand2 = Random.Range(1, listSprite.Count);
        rand3 = Random.Range(0, listSprite.Count);
        rand4 = Random.Range(1, listSprite.Count);


        sprite1.sprite = listSprite[rand1].sprite;
        sprite2.sprite = listSprite[rand2].sprite;
        sprite3.sprite = listSprite[rand3].sprite;
        sprite4.sprite = listSprite[rand4].sprite;

        fromSprite.Add(sprite1);
        fromSprite.Add(sprite2);
        fromSprite.Add(sprite3);
        fromSprite.Add(sprite4);

        toSprite.Add(sprite1);
        toSprite.Add(sprite2);
        toSprite.Add(sprite3);
        toSprite.Add(sprite4);

        //tempI = Mathf.CeilToInt(listPos.Count / 4) * 4 ;

        for (int i = 0; i < listPos.Count - 1; i++)
        {
            defaultScale = new Vector3(0.25f, 0.25f, 1);
            GameObject tempobj = Instantiate(obj, listPos[i], Quaternion.identity);
            listBall.Add(tempobj);
            
            if (i < listPos.Count / 4)
            {
                if (numberOfLayers < 1)
                {
                    listBall[i].transform.localScale = Vector3.zero;
                }
                else
                {
                    listBall[i].GetComponent<SpriteRenderer>().sprite = sprite1.sprite;
                }
                listBall[i].transform.position = new Vector3(listPos[i].x, listPos[i].y, 1);
                listBall[i].transform.SetParent(group1.transform);
            }

            if (i < listPos.Count / 2 + 4 && i >= listPos.Count / 4)
            {
                if (numberOfLayers < 2)
                {
                    listBall[i].transform.localScale = Vector3.zero;
                }
                {
                    listBall[i].GetComponent<SpriteRenderer>().sprite = sprite2.sprite;
                }
                listBall[i].transform.position = new Vector3(listPos[i].x, listPos[i].y, 2);
                listBall[i].transform.SetParent(group2.transform);
            }
            if (i >= listPos.Count / 2 + 4 && i < listPos.Count * 3 / 4 + 3)
            {
                if (numberOfLayers < 3)
                {
                    listBall[i].transform.localScale = Vector3.zero;
                }
                {
                    listBall[i].GetComponent<SpriteRenderer>().sprite = sprite3.sprite;
                }
                listBall[i].transform.position = new Vector3(listPos[i].x, listPos[i].y, 3);
                listBall[i].transform.SetParent(group3.transform);
            }
            if (i >= listPos.Count * 3 / 4 + 3 && i < listPos.Count)
            {
                if (numberOfLayers < 4)
                {
                    listBall[i].transform.localScale = Vector3.zero;
                }
                {
                    listBall[i].GetComponent<SpriteRenderer>().sprite = sprite4.sprite;
                }
                listBall[i].transform.position = new Vector3(listPos[i].x, listPos[i].y, 4);
                listBall[i].transform.SetParent(group4.transform);
            }
        }
        balls1 = group1.GetComponentsInChildren<SpriteRenderer>();
        balls2 = group2.GetComponentsInChildren<SpriteRenderer>();
        balls3 = group3.GetComponentsInChildren<SpriteRenderer>();
        balls4 = group4.GetComponentsInChildren<SpriteRenderer>();

    }

    public void GetListPos()
    {
        float i = -transform.localScale.y;
        float j = -transform.localScale.x;
        float offset = 0.4f;
        while (i < transform.localScale.y)
        {
            j += offset;
            if (j >= transform.localScale.x)
            {
                i += 0.5f;
                j = transform.localScale.x;
                offset = -0.4f;
            }
            else if (j <= -transform.localScale.x)
            {
                offset = 0.4f;
            }
            tempPos.position = new Vector3(transform.position.x + j, transform.position.y + i, 1);
            isSpawn = Physics2D.OverlapCircle(tempPos.position, 0.05f, layerMask);
            if (isSpawn)
            {
                listPos.Add(new Vector3(tempPos.transform.position.x, tempPos.transform.position.y, 1));
            }
        }     
    }

    public void  LayersUpdate()
    {
        Sequence sequence = DOTween.Sequence();
        Sequence sequence1 = DOTween.Sequence();
        if (fromCount >-1 && isFrom)
        {           
            if (numberOfLayers == 1)
            {
                sequence
                    .Append(balls2[balls2.Length - 1].gameObject.transform.DOScale(0f, 0).SetEase(Ease.InQuad))
                    .AppendCallback(() => { balls2[fromCount].gameObject.transform.DOScale(0.3f, 0.2f).SetEase(Ease.OutQuad); })
                    .AppendCallback(() => { balls2[fromCount].gameObject.transform.DOScale(0f, 0.4f).SetEase(Ease.InQuad); });
                lrenderer.setColor(rand2);
                lrenderer.getPosition(balls2[fromCount].transform.position);
            }
            if (numberOfLayers == 2)
            {
                sequence
                    .Append(balls3[balls3.Length - 1].gameObject.transform.DOScale(0f, 0).SetEase(Ease.InQuad))
                    .AppendCallback(() => { balls3[fromCount].gameObject.transform.DOScale(0.3f, 0.2f).SetEase(Ease.OutQuad); })
                    .AppendCallback(() => { balls3[fromCount].gameObject.transform.DOScale(0f, 0.4f).SetEase(Ease.InQuad); });
                lrenderer.setColor(rand3);
                lrenderer.getPosition(balls3[fromCount].transform.position);
            }
            if (numberOfLayers == 3)
            {
                sequence
                    .Append(balls4[balls4.Length - 1].gameObject.transform.DOScale(0f, 0).SetEase(Ease.InQuad))
                    .AppendCallback(() => { balls4[fromCount].gameObject.transform.DOScale(0.3f, 0.2f).SetEase(Ease.OutQuad); })
                    .AppendCallback(() => { balls4[fromCount].gameObject.transform.DOScale(0f, 0.4f).SetEase(Ease.InQuad); });
                lrenderer.setColor(rand4);
                lrenderer.getPosition(balls4[fromCount].transform.position);
            }

            if (numberOfLayers == 0)
            {
                sequence
                    .Append(balls1[balls1.Length - 1].gameObject.transform.DOScale(0f, 0).SetEase(Ease.InQuad))
                    .AppendCallback(() => { balls1[fromCount].gameObject.transform.DOScale(0.3f, 0.2f).SetEase(Ease.OutQuad); })
                    .AppendCallback(() => { balls1[fromCount].gameObject.transform.DOScale(0f, 0.4f).SetEase(Ease.InQuad); });
                lrenderer.setColor(rand1);
                lrenderer.getPosition(balls1[fromCount].transform.position);
            }
            fromCount--;
        }
        if(toCount < tempI && isTo)
        {
            if (numberOfLayers == 1)
            {
                balls1[toCount].sprite = toSprite[0].sprite;
                sequence1
                    .Append(balls1[toCount].gameObject.transform.DOScale(defaultScale * 1.3f, 0.2f).SetEase(Ease.OutQuad))
                    .AppendCallback(() => { balls1[toCount].gameObject.transform.DOScale(defaultScale * 0.1f, 0.1f); })
                    .Append(balls1[toCount].gameObject.transform.DOScale(defaultScale, 0.4f).SetEase(Ease.OutQuad));
                lrenderer.getPosition1(balls1[toCount].transform.position);
            }
            if (numberOfLayers == 2)
            {
                balls2[toCount].sprite = toSprite[1].sprite;
                sequence1
                    .Append(balls2[toCount].gameObject.transform.DOScale(defaultScale * 1.3f, 0.2f).SetEase(Ease.OutQuad))
                    .AppendCallback(() => { balls2[toCount].gameObject.transform.DOScale(defaultScale * 0.1f, 0.1f); })
                    .Append(balls2[toCount].gameObject.transform.DOScale(defaultScale, 0.4f).SetEase(Ease.OutQuad));
                lrenderer.getPosition1(balls2[toCount].transform.position);
            }
            if (numberOfLayers == 3)
            {
                balls3[toCount].sprite = toSprite[2].sprite;
                sequence1
                    .Append(balls3[toCount].gameObject.transform.DOScale(defaultScale * 1.3f, 0.2f).SetEase(Ease.OutQuad))
                    .AppendCallback(() => { balls3[toCount].gameObject.transform.DOScale(defaultScale * 0.1f, 0.1f); })
                    .Append(balls3[toCount].gameObject.transform.DOScale(defaultScale, 0.4f).SetEase(Ease.OutQuad));
                lrenderer.getPosition1(balls3[toCount].transform.position);
            }
            if (numberOfLayers == 4)
            {
                balls4[toCount].sprite = toSprite[3].sprite;
                sequence1
                    .Append(balls4[toCount].gameObject.transform.DOScale(defaultScale * 1.3f, 0.2f).SetEase(Ease.OutQuad))
                    .AppendCallback(() => { balls4[toCount].gameObject.transform.DOScale(defaultScale * 0.1f, 0.1f); })
                    .Append(balls4[toCount].gameObject.transform.DOScale(defaultScale, 0.4f).SetEase(Ease.OutQuad));
                lrenderer.getPosition1(balls4[toCount].transform.position);
            }
            toCount++;
        }
        if (numberOfLayers > 4 || numberOfLayers < 0)
        {
            Debug.Log("NO");
            return;
        }
        
    }

    public void ColorUpdate()
    {
        fromSprite[0].sprite = toSprite[0].sprite;
        fromSprite[1].sprite = toSprite[1].sprite;
        fromSprite[2].sprite = toSprite[2].sprite;
        fromSprite[3].sprite = toSprite[3].sprite;
    }


    private void Update()
    { 
        countDown -= Time.deltaTime;
        if (countDown < 0)
        {
            LayersUpdate();
            countDown = currTime;
        }
        ColorUpdate();
    }
}
